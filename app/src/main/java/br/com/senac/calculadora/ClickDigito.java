package br.com.senac.calculadora;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by sala304b on 15/08/2017.
 */
public class ClickDigito implements View.OnClickListener {

    public TextView resultado;


    public ClickDigito(TextView textView) {
        this.resultado = textView;
    }

    @Override
    public void onClick(View v) {
        String texto = resultado.getText().toString();
        Button botao = (Button) v;
        String digito = botao.getText().toString();

        if (botao.getId() == R.id.limpar) {
            resultado.setText("0");
        } else {

            if (texto.equals("0")) {
                resultado.setText(digito);

            } else {
                if (botao.getId() != R.id.ponto) {
                    resultado.setText(texto + digito);
                } else {
                    if (!texto.contains(".")) {
                        resultado.setText(texto + digito);
                    }
                }
            }
        }
    }
}







