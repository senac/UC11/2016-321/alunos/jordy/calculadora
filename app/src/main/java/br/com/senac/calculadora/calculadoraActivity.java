package br.com.senac.calculadora;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class calculadoraActivity extends AppCompatActivity {
    //private TextView operador;
    private TextView resultado;
    private Button soma;
    private Button subtracao;
    private Button divisao;
    private Button multiplicacao;
    private Button ponto;
    private Button limpar;
    private Button igualdade;
    private Button n0;
    private Button n1;
    private Button n2;
    private Button n3;
    private Button n4;
    private Button n5;
    private Button n6;
    private Button n7;
    private Button n8;
    private Button n9;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculadora);


        this.ponto = (Button)findViewById(R.id.ponto);

        this.n0 = (Button)findViewById(R.id.n0);
        this.n1 = (Button)findViewById(R.id.n1);
        this.n2 = (Button)findViewById(R.id.n2);
        this.n3 = (Button)findViewById(R.id.n3);
        this.n4 = (Button)findViewById(R.id.n4);
        this.n5 = (Button)findViewById(R.id.n5);
        this.n6 = (Button)findViewById(R.id.n6);
        this.n7 = (Button)findViewById(R.id.n7);
        this.n8 = (Button)findViewById(R.id.n8);
        this.n9 = (Button)findViewById(R.id.n9);
        this.limpar = (Button)findViewById(R.id.limpar);

        this.resultado = (TextView)findViewById(R.id.resultado);

        ClickDigito clickDigito = new ClickDigito(this.resultado);


        this.n0.setOnClickListener(clickDigito);
        this.n1.setOnClickListener(clickDigito);
        this.n2.setOnClickListener(clickDigito);
        this.n3.setOnClickListener(clickDigito);
        this.n4.setOnClickListener(clickDigito);
        this.n5.setOnClickListener(clickDigito);
        this.n6.setOnClickListener(clickDigito);
        this.n7.setOnClickListener(clickDigito);
        this.n8.setOnClickListener(clickDigito);
        this.n9.setOnClickListener(clickDigito);
        this.ponto.setOnClickListener(clickDigito);
        this.limpar.setOnClickListener(clickDigito);




        }



    }

